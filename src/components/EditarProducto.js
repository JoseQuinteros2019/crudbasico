import React, { useState, useRef } from 'react';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';



const EditarProducto = (props) => {
    // para editar necesitamos el id
    // necesitamos buscar ese objeto con ese id
    // usamos useRef cuando vamos a modificar algo
    // ref accedemos al input del dom
    //para input defaultValue
    // para check defaultChecked
    const [categoria, setCategoria] = useState("");
    const nombrePlatoRef = useRef('');
    const nombrePrecioRef = useRef('');
    const handleChangeCategoria = e => {
        setCategoria(e.target.value)
  }
  const handleSubmit =async( e) => {
    e.preventDefault();// evita que no se recargue
    // validamos que todos los datos este completo
    let _nombrePlato = nombrePlatoRef.current.value;// accedemos al valor que tiene la variable
    let _precioPlato = nombrePrecioRef.current.value;
    let _categoria = (categoria === '') ? props.producto.categoria : categoria;// toma el valor del props o de state categoria si nosotros los cambiamos
    if (_nombrePlato === ''|| _precioPlato === ''|| _categoria === '')
    {
      alert("Debe completar todos los datos");
      return;
    }
    // si llegamos hasta aqui esta todo bien
    // creamos un objeto que vamos a actualizar en mi restAPI
    const dato = {
      nombrePlato: _nombrePlato,
      precioPlato: _precioPlato,
      categoria:_categoria
    }
    const parametros = {
      method: 'PUT',

     headers: { "Content-Type": "application/json"  },// vienen en el header de mi consulta
      body: JSON.stringify(dato)
    }
    try {
      const url = `http://localhost:4005/bar/${props.producto.id}`;
      const consulta = await fetch(url, parametros);
      console.log(await consulta);


      
      if (consulta.status === 200) {
        Swal.fire(
            'Muy bien!',
            'Se edito  el producto',
            'success'
        )
       
      // cambio la variable que me permite cargar la consultaapi del app.js
      props._setCambiarAPI(true);
      props.history.push('/');// redirecciona a otra pagina
      
  }
    } catch (error) {
      console.log(error);
    }
  }
    return (
        <div>
        <div className="col-md-8 mx-auto ">
                <h1 className="text-center">Editar Producto</h1>
                
        
          <form className="mt-5" onSubmit={handleSubmit} >
            <div className="form-group">
              <label>Nombre del Plato</label>
                        <input
                            type="text"
                            className="form-control"
                            name="nombre"
                            placeholder="Nombre del Plato"
                            ref={nombrePlatoRef}
                            defaultValue={props.producto.nombrePlato}
               
              />
            </div>
  
            <div className="form-group">
              <label>Precio del Plato</label>
              <input
                type="number"
                className="form-control"
                name="precio"
                            placeholder="Precio del Plato"
                           ref={nombrePrecioRef}
                           defaultValue={props.producto.precioPlato}
              />
            </div>
  
            <h3 className="text-center">Categoría:</h3>
            <div className="text-center">
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="categoria"
                                value="postre"

                                onChange={handleChangeCategoria}
                                defaultChecked={props.producto.categoria==='postre'}
                  
                />
                <label className="form-check-label">Postre</label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="categoria"
                                value="bebida"
                                onChange={handleChangeCategoria}
                                defaultChecked={props.producto.categoria==='bebida'}
                />
                <label className="form-check-label">Bebida</label>
              </div>
  
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="categoria"
                  value="principal"
                                onChange={handleChangeCategoria}
                                defaultChecked={props.producto.categoria==='principal'}
                />
                <label className="form-check-label">Principal</label>
              </div>
  
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="categoria"
                  value="ensalada"
                                onChange={handleChangeCategoria}
                                defaultChecked={props.producto.categoria==='ensalada'}
                />
                <label className="form-check-label">Ensalada</label>
              </div>
            </div>
  
            <button
              type="submit"
              className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3"
            >
              Editar Producto
            </button>
          </form>
        </div>
      </div>
    );
};

export default withRouter(EditarProducto);
