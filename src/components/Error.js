import React from "react";

const Error = (props) => {
  return (
    <div className={props.claseAlert} role="alert">
      {props.mensaje}
    </div>
  );
};

export default Error;
