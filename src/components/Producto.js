import React from "react";
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';


const Producto = props => {
  // Link tiene propiedades que me permite navegar entre paginas
  // usamos ` dento de link por que usamos variable dentro de un texto
  const eliminarProducto = async (id) => {
    console.log(id);
    try {
      const parametros = {
        method: 'DELETE',

       headers: { "Content-Type": "application/json"  },// vienen en el header de mi consulta
      
    }
    const consulta = await fetch(`http://localhost:4005/bar/${id} `, parametros);
    if (consulta.status === 200) {
      Swal.fire({
        title: 'Estas seguro?',
        text: "No podras recuperar el archivo!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminalo!'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Borrado',
            'Se elimino tu producto.',
            'success'
          )
          props._setCambiarAPI(true);
        }
      })
      
    // cambio la variable que me permite cargar la consultaapi del app.js
    
    
}
    
    } catch (error) {
      console.log(error)
    }
  }
  return (
      <li class="list-group-item d-flex justify-content-between">
      <div>
        
              Plato: {props.items.nombrePlato} <span className="font-weight-bold"> ${props.items.precioPlato}</span>
              </div>
      <div>
       <Link to={`/productos/editar/${props.items.id}`}> <button type="button" className="btn btn-primary mx-2 ">
          Editar
        </button></Link>
        <button type="button" className="btn btn-danger   mx-2" onClick={()=>eliminarProducto(props.items.id)}>
          Eliminar
        </button>
      </div>
    </li>
  );
};

export default Producto;
