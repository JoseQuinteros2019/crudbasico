import React, { useState } from 'react';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';
const AgregarProducto = (props) => {
    const [nombre, setNombre] = useState("");
    const [precio, setPrecio] = useState('');
    const [categoria, setCategoria] = useState("");
    const handleChangeCategoria = e => {
        setCategoria(e.target.value)
    }
    const handleSubmit =async (e) => {
        e.preventDefault();
        // validar que todos los campos sean cargados
        if (nombre === '' || precio === '' || categoria === '')
        { 
            alert("debe completar  todos los datos");
            return;// aqui termina la ejecucion si no se cargaron todos los datos
        }
        // aqui vamos  a guardar el nuevo producto
        try {
            const dato = {
                nombrePlato: nombre,
                precioPlato: precio,
                categoria: categoria
            }
            console.log(JSON.stringify(dato));
            const parametros = {
                method: 'POST',

               headers: { "Content-Type": "application/json"  },// vienen en el header de mi consulta
                body: JSON.stringify(dato)
            }
            const consulta = await fetch("http://localhost:4005/bar", parametros);
            const respuesta = await consulta.json(consulta);
            console.log(consulta.status);
            console.log(respuesta);
            if (consulta.status === 201) {
                Swal.fire(
                    'Muy bien!',
                    'Se agrego un nuevo producto',
                    'success'
                )
                setNombre('');
              setPrecio('');
              // cambio la variable que me permite cargar la consultaapi del app.js
              props._setCambiarAPI(true);
              props.history.push('/');// redirecciona a otra pagina
              
          }
          
        } catch (error) {
            console.log(error);
        }
        

        
        
    }

    return (
        <div>
        <div className="col-md-8 mx-auto ">
                <h1 className="text-center">Agregar Nuevo Producto</h1>
                
        
          <form className="mt-5" onSubmit={handleSubmit} >
            <div className="form-group">
              <label>Nombre del Plato</label>
                        <input
                            type="text"
                            className="form-control"
                            name="nombre"
                            placeholder="Nombre del Plato"
                            onChange={(e) => setNombre(e.target.value)}
                            value={nombre}
               
              />
            </div>
  
            <div className="form-group">
              <label>Precio del Plato</label>
              <input
                type="number"
                className="form-control"
                name="precio"
                            placeholder="Precio del Plato"
                            onChange={(e)=>setPrecio(e.target.value)}
                            value={precio}
               
              />
            </div>
  
            <h3 className="text-center">Categoría:</h3>
            <div className="text-center">
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="categoria"
                                value="postre"
                                onChange={handleChangeCategoria}
                  
                />
                <label className="form-check-label">Postre</label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="categoria"
                                value="bebida"
                                onChange={handleChangeCategoria}
                 
                />
                <label className="form-check-label">Bebida</label>
              </div>
  
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="categoria"
                  value="principal"
                  onChange={handleChangeCategoria}
                />
                <label className="form-check-label">Principal</label>
              </div>
  
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="categoria"
                  value="ensalada"
                  onChange={handleChangeCategoria}
                />
                <label className="form-check-label">Ensalada</label>
              </div>
            </div>
  
            <button
              type="submit"
              className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3"
            >
              Agregar Producto
            </button>
          </form>
        </div>
      </div>
    );
};
export default withRouter(AgregarProducto);








