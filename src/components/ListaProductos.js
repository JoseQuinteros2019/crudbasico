import React, { useState, useEffect } from "react";
import Producto from "./Producto";

const ListaProductos = props => {
  return (
    <div className="container">
      <h1>Lista de Productos</h1>
      <ul class="list-group">
        {props.arreglodeProductos.map((dato, id) => (
          <Producto items={dato} key={id} _setCambiarAPI={props._setCambiarAPI}  />
        ))}
      </ul>
    </div>
  );
};

export default ListaProductos;
