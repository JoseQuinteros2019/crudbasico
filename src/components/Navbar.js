import React from "react";
import { BrowserRouter ,Link, NavLink } from "react-router-dom";

const Navbar = () => {
  return (
      <div>
          
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link exact={true} to="/">
          
          <span className="navbar-brand mb-0 h1">CrudBasico</span>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <NavLink exact={true} className="nav-link" activeClassName="active" to="/">Productos</NavLink>
            </li>
            <li className="nav-item">
              <NavLink exact={true} className="nav-link" activeClassName="active"  to="/productos/nuevo">Agregar Productos</NavLink>
            </li>
          </ul>
        </div>
              </nav>
              
    </div>
  );
};

export default Navbar;
