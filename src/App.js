import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import ListaProductos from "./components/ListaProductos";
import EditarProductos from "./components/EditarProducto";
import AgregarProducto from "./components/AgregarProducto";
import Producto from "./components/Producto";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";

function App() {
  // <Route exact path="/productos/editar/:id" component={EditarProductos} ></Route> los dos puntos me dice que viene un parametro
  // creamos un state
  const [productos, setProductos] = useState([]);
  const [cambiarAPI, setCambiarAPI] = useState([true]);
  //ejecutamos el ciclo de vida
  useEffect(() => {
    if (cambiarAPI) {
      consultarAPI();
      setCambiarAPI(false);
    }
  }, [cambiarAPI]); // adentro del corchete se pone que queremos que vea react para ejecutar useEffect

  // funcion que consulta a la appi
  const consultarAPI = async () => {
    try {
      // camino normal
      const consulta = await fetch("http://localhost:4005/bar"); // hacemos una operacion get aqui hay q cambiar el puerto si usamos otro puerto
      const respuesta = await consulta.json();
      console.log(consulta);
      console.log(respuesta);
      setProductos(respuesta); // aqui guardo datos en el state producto
    } catch (error) {
      // despues del catch va un parametro
      console.log(error);
    }
  };

  // cuando le paso props a un componente cambiar render por component
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
          <Route
            exact
            path="/"
            render={() => <ListaProductos arreglodeProductos={productos} _setCambiarAPI={setCambiarAPI} />}
          ></Route>
          <Route
            exact
            path="/productos/nuevo"
            render={() => <AgregarProducto _setCambiarAPI={setCambiarAPI} />}
          ></Route>
          <Route
            exact
            path="/productos/editar/:id"
            render={props => {
              console.log(props.match.params.id);
              let identificadoProducto = parseInt(props.match.params.id); //obtengo el id del producto para editar
              console.log(identificadoProducto);
              // buscar el producto
              let productoEncontrado = productos.filter(
                prod => prod.id === identificadoProducto
              );
              console.log(productoEncontrado[0]);

              //usamos return por que tenemos varias lineas de codigo
              return <EditarProductos producto={productoEncontrado[0]} _setCambiarAPI={setCambiarAPI} />;
            }}
          ></Route>
        </Switch>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
